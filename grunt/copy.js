var pages = {
		expand: true
		, cwd: 'src/pages'
		, src: ['**']
		, dest: 'target/'
	};
var scripts = {
	expand: true
	, cwd: 'src/js'
	, src: ['**']
	, dest: 'target/js'
};
var thirdScripts = {
	expand: true
	, cwd: 'bower_components'
	, src: [
		]
	, dest: 'target/js'
	, flatten: true
};
var images = {
	expand: true
	, cwd: 'src/images'
	, src: ['**']
	, dest: 'target/img'
	, flatten: true
};
var files = [
	pages
	, scripts
	, thirdScripts
	, images
];

module.exports = {
	dist: {
		files: files
	}
	, serve: {
		files: []
	}
};