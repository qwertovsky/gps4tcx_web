const sass = require('node-sass');
module.exports = {
	// development version
	serve: {
		options: {
			outputStyle: 'nested'
			, sourceMap: true
			, implementation: sass
		}
		, files: [{
			expand: true
			, cwd: 'src/styles'
			, src: ['*.scss']
			, dest: '.tmp/styles'
			, ext: '.css'
		}]
	}
	// production version
	, dist: {
		options: {
			outputStyle: 'compressed'
			, sourceMap: false
			, implementation: sass
		}
		, files: [{
			expand: true
			, cwd: 'src/styles'
			, src: ['*.scss']
			, dest: '<%= config.dist %>/styles'
			, ext: '.css'
		}]
	}
};