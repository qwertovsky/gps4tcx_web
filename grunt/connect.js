module.exports = {
    options: {
        port: 9000
        , open: false
        , livereload: '<%= config.liveReloadPort %>'
        , hostname: 'localhost'
        , base: '/'
    }
	, serve : {
		options : {
			middleware : function(connect) {
				return [
						connect.static('.tmp'),
						connect().use('/js', connect.static('./src/js')),
						connect().use('/img', connect.static('src/images')),
            connect().use('/', connect.static('src/pages'))
					  ];
			}
		}
	}
  , dist: {
  	options: {
  		keepalive: true
  		, base: '<%= config.dist %>'
  	}
  }
};
