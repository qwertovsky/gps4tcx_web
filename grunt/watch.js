module.exports = {
  styles: {
    files: [
        'src/styles/*.scss'
    ],
    tasks: [
        'sass:serve'
    ]
  }
  , livereload: {
    options: {
        livereload: '<%= config.liveReloadPort %>'
    },
    files: [
        '<%= config.app %>/**/*.html',
        '<%= config.app %>/**/*.js',
        '.tmp/styles/*.css',
        '<%= config.app %>/images/*.{png,jpg,jpeg,gif,webp,svg}'
    ]
  }
};