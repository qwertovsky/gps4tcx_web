module.exports = function(grunt, options){
  return {
		"dist":
		{
			"description": "Build"
			, "tasks": [
        "clean:dist",
			  "sass:dist",
			  "copy:dist"
			]
		}
		, "serve":
		{
			"description": "Build then start a connect web server"
			, "tasks": function(target) {
				if (target === 'dist') {
					grunt.task.run(["dist", "connect:dist"]);
		    } else {
			    	if (target) {
			    		grunt.log.warn('Target `' + target + '` is undefined for ' + this.name);
			    	}
					  grunt.task.run([
  						"clean:serve",
  						"sass:serve",
  						"connect:serve",
  						"watch"
					  ]);
			  }
			}
		}
	}
}