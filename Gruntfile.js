module.exports = function(grunt) {
	var config = {
		dist: "target"
		,liveReloadPort: 35729
		, app: "src"
	};

	require('time-grunt')(grunt);
	require('load-grunt-config')(grunt);

	grunt.config.merge({
		config: config
	});
};
